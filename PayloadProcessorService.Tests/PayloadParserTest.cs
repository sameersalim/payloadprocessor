﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mi9.PayloadProcessorService;
using System.Collections.Generic;
using Mi9.PayloadProcessorService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mi9.PayloadProcessorService.BusinessLayer;

namespace PayloadProcessorService.Tests.BusinessLayer
{
    [TestClass]
    public class PayloadParserTest
    {
        [TestMethod]
        public void ParseValidJson()
        {
            // Arrange
            string jsondata = @"{
                                    'payload': [
                                        {
                                            'slug': 'show/seapatrol',
                                            'title': 'Sea Patrol',
                                            'tvChannel': 'Channel 9'
                                        }
	                                ],
                                    'skip': 0,
                                    'take': 10,
                                    'totalRecords': 75
                                }";
            IPayloadParser payloadParser = new PayloadParser();

            // Act
            RootObject rootobj = payloadParser.ParseInput(jsondata);

            // Assert
            Assert.IsNotNull(rootobj);
            Assert.AreEqual(1, rootobj.payload.Count);
            Assert.AreEqual(10, rootobj.take);
            Assert.AreEqual(75, rootobj.totalRecords);
        }

        [TestMethod]
        [ExpectedException(typeof(PayloadException),"Could not decode request: JSON parsing failed")]
        public void ParseInValidJson_Exception()
        {
            // Arrange
            string jsondata = @"{
                                    'payload': [
                                        {
                                            'slug': 'show/seapatrol',
                                            'title': 'Sea Patrol',
                                            'tvChannel': 'Channel 9'
                                        }
	                                ],
                                    'skip': 0,sasasa
                                    'take': 10,
                                    'totalRecords': 75
                                }";
            IPayloadParser payloadParser = new PayloadParser();

            // Act
            RootObject rootobj = payloadParser.ParseInput(jsondata);

            // Assert
            // Exception expected
        }

        [TestMethod]
        [ExpectedException(typeof(PayloadException), "Could not decode request: JSON parsing failed")]
        public void ParseInValidJsonFormat_Exception()
        {
            // Arrange
            string jsondata = @"This is a test app.";
            IPayloadParser payloadParser = new PayloadParser();

            // Act
            RootObject rootobj = payloadParser.ParseInput(jsondata);

            // Assert
            // Exception expected
        }

        [TestMethod]
        public void ParseValidJsonData()
        {
            // Arrange
            #region json data string
            string jsondata = @"{
                                 'payload': [
                                                {
                                                    'country': 'UK',
                                                    'description': 'Whats life like when you have enough children to field your own football team?',
                                                    'drm': true,
                                                    'episodeCount': 3,
                                                    'genre': 'Reality',
                                                    'image': {
                                                        'showImage': 'http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg'
                                                    },
                                                    'language': 'English',
                                                    'nextEpisode': null,
                                                    'primaryColour': '#ff7800',
                                                    'seasons': [
                                                        {
                                                            'slug': 'show/16kidsandcounting/season/1'
                                                        }
                                                    ],
                                                    'slug': 'show/16kidsandcounting',
                                                    'title': '16 Kids and Counting',
                                                    'tvChannel': 'GEM'
                                                }
                                    ],
                                    'skip': 0,
                                    'take': 10,
                                    'totalRecords': 75
                                }";
            #endregion 

            IPayloadParser payloadParser = new PayloadParser();

            // Act
            RootObject rootobj = payloadParser.ParseInput(jsondata);
            JsonResponse response = payloadParser.ProcessInput(rootobj);

            // Assert
            Assert.IsNotNull(rootobj);
            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.response.Count);
            Assert.AreEqual("http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg", response.response[0].image);
            Assert.AreEqual("show/16kidsandcounting", response.response[0].slug);
            Assert.AreEqual("16 Kids and Counting", response.response[0].title);
        }
    }
}
