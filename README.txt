How to use payload processor webservice:
--------------------------------------------
<deployment-path>/api/PayloadProcessor

eg, http://mi9codingtest.apphb.com/api/PayloadProcessor

localhost, http://localhost:<port-number>/api/PayloadProcessor


Test cases
-----------
Unit test cases are available in PayloadProcessorService.Tests folder 

How to run unit tests
----------------------
Unit test cases can be executed directly from visual studio test explorer



