﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mi9.PayloadProcessorService.Models
{
    /// <summary>
    /// root object class
    /// </summary>
    public class RootObject
    {
        public List<Payload> payload { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
        public int totalRecords { get; set; }
    }
}