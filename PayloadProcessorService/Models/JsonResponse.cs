﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mi9.PayloadProcessorService.Models
{
    public class JsonResponse
    {
        public List<ResponseObject> response { get; set; }
    }
}