﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mi9.PayloadProcessorService.Models
{
    /// <summary>
    /// Invalid request class
    /// </summary>
    public class InvalidRequest
    {
        public string error { get; set; }
    }
}