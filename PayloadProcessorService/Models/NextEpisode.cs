﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mi9.PayloadProcessorService.Models
{
    /// <summary>
    /// Next episode class
    /// </summary>
    public class NextEpisode
    {
        public object channel { get; set; }
        public string channelLogo { get; set; }
        public object date { get; set; }
        public string html { get; set; }
        public string url { get; set; }
    }
}