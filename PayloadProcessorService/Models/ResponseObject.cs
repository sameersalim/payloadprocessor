﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mi9.PayloadProcessorService.Models
{
    /// <summary>
    /// Response object class
    /// </summary>
    public class ResponseObject
    {
        public string image { get; set; }
        public string slug { get; set; }
        public string title { get; set; }
    }
}