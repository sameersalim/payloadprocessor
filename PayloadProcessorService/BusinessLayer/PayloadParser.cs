﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using Mi9.PayloadProcessorService.Models;

namespace Mi9.PayloadProcessorService.BusinessLayer
{
    /// <summary>
    /// payload parser class
    /// </summary>
    public class PayloadParser : IPayloadParser
    {
        /// <summary>
        /// parse input implementation
        /// </summary>
        /// <param name="inputRequest"></param>
        /// <returns></returns>
        public RootObject ParseInput(dynamic inputRequest)
        {
            try
            {
                return Json.Decode(inputRequest.ToString(), typeof(RootObject));
            }
            catch
            {
                throw new PayloadException("Could not decode request: JSON parsing failed");
            }
        }

        /// <summary>
        /// process input implementation
        /// </summary>
        /// <param name="rootobj"></param>
        /// <returns></returns>
        public JsonResponse ProcessInput(RootObject rootobj)
        {
            var jsonresponse = new JsonResponse();
            if (rootobj != null && rootobj.payload != null)
            {
                jsonresponse.response = new List<ResponseObject>();
                rootobj.payload.ForEach(item =>
                    {
                        if ((item.drm == true) && item.episodeCount > 0)
                        {
                            jsonresponse.response.Add(new ResponseObject
                            {
                                image = item.image != null ? item.image.showImage : string.Empty,
                                slug = item.slug,
                                title = item.title
                            });
                        }
                    });
            }
            return jsonresponse;
        }
    }
}