﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mi9.PayloadProcessorService.Models;

namespace Mi9.PayloadProcessorService.BusinessLayer
{
    /// <summary>
    /// payload parser interface
    /// </summary>
    public interface IPayloadParser
    {
        /// <summary>
        /// parse input 
        /// </summary>
        /// <param name="inputRequest">json data</param>
        /// <returns>root object class</returns>
        RootObject ParseInput(dynamic inputRequest);

        /// <summary>
        /// process input
        /// </summary>
        /// <param name="rootobj">root object to process</param>
        /// <returns>list of response objects</returns>
        JsonResponse ProcessInput(RootObject rootobj);

    }
}