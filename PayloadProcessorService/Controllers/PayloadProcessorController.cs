﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Helpers;
using Mi9.PayloadProcessorService.Models;
using Mi9.PayloadProcessorService.BusinessLayer;
using System.Web.Script.Serialization;

namespace Mi9.PayloadProcessorService.Controllers
{
    public class PayloadProcessorController : ApiController
    {
        /// <summary>
        /// Parse payload
        /// </summary>
        /// <param name="inputRequest">Json data</param>
        /// <returns>filtered output</returns>
        [HttpPost]
        public HttpResponseMessage ParsePayload(dynamic inputRequest)
        {
            if (inputRequest != null)
            {
                try
                {
                    IPayloadParser payloadParser = new PayloadParser();

                    // parse input
                    RootObject payloads = payloadParser.ParseInput(inputRequest);

                    // process input
                    var jsonresponse = payloadParser.ProcessInput(payloads);

                    // send response
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse, "application/json");

                }
                catch (PayloadException exception)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                           new InvalidRequest { error = string.Format("Could not decode request: {0}", exception.Message) }, "application/json");
                }
                catch (Exception exception)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,
                           new InvalidRequest { error = string.Format("Could not decode request: {0}", exception.Message) }, "application/json");
                }
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest,
                    new InvalidRequest { error = "Could not decode request: JSON parsing failed" }, "application/json");
        }

    }
}
