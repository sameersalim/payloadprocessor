﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mi9.PayloadProcessorService
{
    /// <summary>
    /// payload exception handling
    /// </summary>
    public class PayloadException : Exception
    {
        public PayloadException(string message)
            : base(message)
        { }

        public PayloadException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}